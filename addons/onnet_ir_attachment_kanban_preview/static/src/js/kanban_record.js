odoo.define('onnet_ir_attachment_kanban_preview.KanbanRecord', function (require) {
'use strict';

var KanbanRecord = require('web.KanbanRecord');
var Dialog = require('web.Dialog');

KanbanRecord.include({
    //--------------------------------------------------------------------------
    // Private
    //--------------------------------------------------------------------------

    /**
     * @override
     * @private
     */
    _openRecord: function () {
        if (this.modelName === 'ir.attachment') {
            var types = ['pdf', 'image'];
            if (this.recordData.mimetype.match(types.join('|'))) {
                let src_url;
                if (this.recordData.mimetype === "application/pdf") {
                    src_url = "/web/static/lib/pdfjs/web/viewer.html?file=/web/content/" + this.recordData.id + "?filename=" + window.encodeURIComponent(this.name);
                } else {
                    src_url = "/web/image/" + this.recordData.id;
                }
                var dialog = new Dialog(this, {
                    title: this.recordData.name,
                    $content: $('<iframe height="800" src=' + src_url + '/>')
                });
                dialog.open();
            }else{
                this._super.apply(this, arguments);
            }
        }else{
            this._super.apply(this, arguments);
        }
    }
});

});