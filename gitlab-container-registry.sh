source .env
gitlab_login(){
  echo "Login to registry.gitlab.com ..."
  docker login registry.gitlab.com
}

register_container() {
  echo -n "Enter your gitlab registry (e.g: registry.gitlab.com/<GROUP_NAME>/<PROJECT_NAME>: "
  read registry
  echo "Creating docker images ..."
  docker build -t ${registry} ./dockerfiles/odoo/${ODOO_VERSION}
  docker build -t ${registry} ./dockerfiles/postgres/${POSTGRES_VERSION}
}

push_container() {
  echo "Pushing docker images to gitlab ..."
  docker push ${registry}/odoo${ODOO_VERSION}
  docker push ${registry}/postgres${POSTGRES_VERSION}
}

gitlab_login
register_container
push_container