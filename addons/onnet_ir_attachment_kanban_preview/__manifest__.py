# -*- coding: utf-8 -*-
{
    'name': "Onnet - Attachment Kanban Preview PDF / Image",
    'depends': ['base'],
    'sequence': 31,
    'category': 'base',
    'summary': " Attachment Kanban Preview PDF / Image",
    'author': "LongDT",
    'website': "https://on.net.vn",
    'version': '0.1',
    # always loaded
    'data': [
        'views/assets.xml',
    ],
    'qweb': [
    ],
    'installable': True,
    'application': True,
}
